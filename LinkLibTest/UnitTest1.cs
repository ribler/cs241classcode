﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Diagnostics;
using LinkLib;

namespace LinkLibTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void createANode()
        {
            Node<int> node = new Node<int>(10, null);
            Assert.AreEqual(10, node.Item);
            Assert.IsNull(node.Next);
        }

        [TestMethod]
        public void addNode()
        {
            Node<int> node = new Node<int>(10, null);
            Node<int> newList = node.Add(9);
            Assert.AreEqual(9, newList.Item);
            Assert.AreEqual(10, newList.Next.Item);
        }

        [TestMethod]
        public void vector()
        {
            Vector<int> myVector = new Vector<int>();
            myVector.Add(10);
            Assert.AreEqual(10, myVector[0]);
        }

        [TestMethod]
        public void enumerateVector()
        {
            List<int> myList = new List<int>();
            Vector<int> myVector = new Vector<int>();
            myList.Add(1);
            myVector.Add(1);
            myList.Add(2);
            myVector.Add(2);
            myList.Add(3);
            myVector.Add(3);
            myList.Add(4);
            myVector.Add(4);

            int listSum = 0;
            // For every item in myList, add
            // the item to the sum.
            foreach (int x in myList)
            {
                listSum += x;
            }

            int vectorSum = 0;
            foreach (int y in myVector)
            {
                vectorSum += y;
            }

            Assert.AreEqual(listSum, vectorSum);
        }

        [TestMethod]
        public void reverseEnumeratorTest()
        {
            int[] testData = new int[] { 10, 20, 30, 40, 55 };

            Vector<int> myVector = new Vector<int>();

            foreach (int x in testData)
            {
                myVector.Add(x);
            }

            int[] result = new int[testData.Length];
            Array.Copy(testData, result, testData.Length);
            Array.Reverse(result);

            Stopwatch timer = new Stopwatch();
            timer.Start();
            int index = 0;
            foreach (int r in
                new ReverseVectorEnumerator<int>(myVector))
            {
                Assert.AreEqual(result[index++], r);
            }
            timer.Stop();

        }

            // Stack data type
            //   Abstract Data Type
            // Operations that define the data type
            // 1) Create a new stack
            //   new stacks are empty (contains no items)
            //  2) Push an item onto the stack
            //  3) Pop an item off of the stack
            //  4) Determine the number of items in the stack
            //  5) Determine if the stack is empty

        [TestMethod]
        public void builtInStackTest()
        {
            Stack<int> someStack = new Stack<int>();

            int[] myArray = new int[] { 10, 20, 30, 40, 50 };
            foreach(int x in myArray)
            {
                someStack.Push(x);
            }

            int[] reversed = new int[myArray.Length];
            Array.Copy(myArray, reversed, myArray.Length);
            Array.Reverse(reversed);
            int index = 0;
            while(someStack.Count > 0)
            {
                int topValue = someStack.Pop();
                Assert.AreEqual(reversed[index++], topValue);
            }
        }

        [TestMethod]
        public void multiplyTest()
        {
            int[] myArray = new int[] { 1, 2, 3, 5, 1 };

            int sum = 0;
            for(int i=0;i< myArray.Length; i++)
            {
                for(int j=i+1; j<myArray.Length; j++)
                {
                    sum = sum + myArray[i] * myArray[j];
                }
            }
        }

        [TestMethod]
        public void tupleTest()
        {
            Tuple<float, float> myPoint = new
                Tuple<float, float>(10.0f,
                20.0f);

            Assert.AreEqual(10.0f, myPoint.Item1);
            Assert.AreEqual(20.0f, myPoint.Item2);

            Tuple<int, int, int> myTriple =
                new Tuple<int, int, int>(10, 20, 30);

            Assert.AreEqual(10, myTriple.Item1);
            Assert.AreEqual(20, myTriple.Item2);
            Assert.AreEqual(30, myTriple.Item3);

            List<Tuple<int, int, int>> listOfTuples =
                new List<Tuple<int, int, int>>();

            listOfTuples.Add(new Tuple<int, int, int>(10, 20, 30));
            listOfTuples.Add(new Tuple<int, int, int>(100, 200, 300));

            foreach(Tuple<int, int, int> x in listOfTuples)
            {
                Assert.IsTrue(x.Item1 == 10 || x.Item1 == 100);
            }
        }

        [TestMethod]
        public void randomExample()
        {
            // Create an instance of the Random class
            // The Random constructor that takes
            // a parameter allows you to specify
            // the pseudo-random sequence you want
            // to generate.
            int seed = 23423511;
            Random random = new Random(seed);

            Random anotherRandom =
                new Random(seed);

            for(int i=0;i<1024;i++)
            {
                Assert.AreEqual(random.Next(),
                    anotherRandom.Next());
            }

            // Generate a random sequence 
            // of n numbers in the range
            // [0, magnitudeLimit).
            int n = 1024;
            int[] myArray = new int[n];
            int magnitudeLimit = 11;
            for(int i=0;i<n;i++)
            {
               myArray[i] = 
               random.Next() % magnitudeLimit;
            }

            int[] negativeArray = new int[n];
            int bias = magnitudeLimit / 2;
            for (int i = 0; i < n; i++)
            {
                negativeArray[i] =
                (random.Next() % magnitudeLimit) - bias;
            }

        }
    }
}
