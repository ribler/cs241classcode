﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SymbolTable;
using System.IO;

namespace SymbolTableTest
{
    [TestClass]
    public class UnitTest1
    {
        public void readFileIntoSymbolTable(string filename,
            ISymbolTable<string, int> symbolTable)
        {
            StreamReader reader = new
                StreamReader(filename);
            string line;
            string sDelimiters = "~!@#$%^&*()_+,.?<>/ \t";
            char[] delimiters = sDelimiters.ToCharArray();
            while(!reader.EndOfStream)
            {
                line = reader.ReadLine();
                string[] words = line.Split(delimiters,
                    StringSplitOptions.RemoveEmptyEntries);
                foreach(string word in words)
                {
                    if(symbolTable.contains(word))
                    {
                        int count = symbolTable.get(word);
                        symbolTable.put(word, count + 1);
                    }
                    else
                    {
                        symbolTable.put(word, 1);
                    }
                }
            }
        }

        [TestMethod]
        public void TestMethod1()
        {
            ArraySymbolTable<string, int> milesTo =
                new ArraySymbolTable<string, int>();

            milesTo.put("Roanoke", 44);
            milesTo.put("San Francisco", 2350);
            milesTo.put("Orlando", 628);
            milesTo.put("Baltimore", 189);

            int totalMiles = 0;
            foreach(string key in milesTo.keys())
            {
                //Console.WriteLine("{0} miles to {1} from Lynchburg",
                 //   key, milesTo.get(key));
                totalMiles += milesTo.get(key);
            }
            Assert.AreEqual(44 + 2350 + 628 + 189, totalMiles);
            Assert.AreEqual(44, milesTo.get("Roanoke"));
            Assert.AreEqual(2350, milesTo.get("San Francisco"));
            Assert.AreEqual(628, milesTo.get("Orlando"));
            Assert.AreEqual(189, milesTo.get("Baltimore"));
        }
    }
}
