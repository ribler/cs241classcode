﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SymbolTable
{
    class ArraySymbolTableRecord<KeyType, ValueType> 
        : IComparable where KeyType : IComparable<KeyType>
    {
        public ArraySymbolTableRecord(KeyType key,
            ValueType value)
        {
            Key = key;
            Value = value;
        }

        public int CompareTo(object obj)
        {
            KeyType rhs = (KeyType) obj;
            return Key.CompareTo(rhs);
        }

        public KeyType Key { get; private set; }
        public ValueType Value { get; set; }
    }
}
