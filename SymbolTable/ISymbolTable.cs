﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SymbolTable
{
    // Restriction on ADT ISymbolTable:
    //   There is only one value for each key.
    public interface ISymbolTable<KeyType, ValueType>
    {
        ValueType get(KeyType key);

        // The put operation will replace an 
        // existing entry with the same key, or
        // if the key does not already exist in the
        // symbol table, add a new entry with this key.
        void put(KeyType key, ValueType value);

        // Remove the key and its data from the 
        // SymbolTable
        void delete(KeyType key);

        // Return true if the key already exists
        // in the symbol table.
        bool contains(KeyType key);

        // isEmpty returns true when there are no
        // items in the symbol table.
        bool isEmpty();


        // Size() returns the number of key-value
        // pairs in the symbol table.
        int size();

        // All the keys in the table.
        IEnumerable<KeyType> keys();
    }
}
