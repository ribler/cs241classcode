﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SymbolTable
{
    public class ArraySymbolTable<KeyType, ValueType> :
        ISymbolTable<KeyType, ValueType> where KeyType : IComparable<KeyType>
    {
        public bool contains(KeyType key)
        {
            bool found = false;
            for (int i = 0; i < theTable.Count && !found; i++)
            {
                if (theTable[i].Key.CompareTo(key) == 0)
                {
                    found = true;
                }
            }
            return found;
        }

        public void delete(KeyType key)
        {
            bool found = false;
            for (int i = 0; i < theTable.Count && !found; i++)
            {
                if (theTable[i].Key.CompareTo(key) == 0)
                {
                    theTable.RemoveAt(i);
                    found = true;
                }
            }
        }

        public ValueType get(KeyType key)
        {
            bool found = false;
            for (int i = 0; i < theTable.Count && !found; i++)
            {
                if (theTable[i].Key.CompareTo(key) == 0)
                {
                    return theTable[i].Value;
                }
            }
            throw new ApplicationException(
                    String.Format("ArraySymbolTable.get {0} not found.",
                    key));
        }

        public bool isEmpty()
        {
            return theTable.Count == 0;
        }

        public IEnumerable<KeyType> keys()
        {
            foreach (ArraySymbolTableRecord<KeyType, ValueType> record in theTable)
            {
                yield return record.Key;
            }
        }

        public void put(KeyType key, ValueType value)
        {
            // put on an existing key should replace the existing key.
            bool found = false;
            for (int i = 0; i < theTable.Count && !found; i++)
            {
                if (theTable[i].Key.CompareTo(key) == 0)
                {
                    theTable[i].Value = value;
                    found = true;
                }
            }

            if (!found)
            {
                theTable.Add(
                    new ArraySymbolTableRecord<KeyType, ValueType>(key, value));
            }
        }

        public int size()
        {
            return theTable.Count;
        }

        private List<ArraySymbolTableRecord<KeyType, ValueType>> theTable
            = new List<ArraySymbolTableRecord<KeyType, ValueType>>();
    }
}
