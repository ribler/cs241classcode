﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace LinkLib
{
    public class ReverseVectorEnumerator<Type> : 
        IEnumerator<Type>, IEnumerable<Type>
    {
        public ReverseVectorEnumerator(Vector<Type> vector)
        {
            theVector = vector;
            currentIndex = vector.Length();
        }

        Type IEnumerator<Type>.Current
        {
            get { return theVector[currentIndex]; }
        }

        object IEnumerator.Current
        {
            get { return theVector[currentIndex]; }
        }


        void IDisposable.Dispose()
        {
        }

        bool IEnumerator.MoveNext()
        {
            --currentIndex;
            return currentIndex >= 0;
        }

        void IEnumerator.Reset()
        {
            currentIndex = theVector.Length();
        }

        IEnumerator<Type> IEnumerable<Type>.GetEnumerator()
        {
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this;
        }

        private int currentIndex;
        private Vector<Type> theVector;
    }
}
