﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace LinkLib
{
    class ForwardVectorEnumerator<Type> : 
        IEnumerator<Type>
    {
        public ForwardVectorEnumerator(Vector<Type> vector)
        {
            theVector = vector;
            currentIndex = -1;
        }

        Type IEnumerator<Type>.Current
        {
           get { return theVector[currentIndex];  }
        }

        object IEnumerator.Current
        {
           get { return theVector[currentIndex];  }
        }

        void IDisposable.Dispose()
        {
        }

        bool IEnumerator.MoveNext()
        {
          ++currentIndex;
          return currentIndex < theVector.Length();
        }

        void IEnumerator.Reset()
        {
        currentIndex = -1;
        }

        private int currentIndex;
        private Vector<Type> theVector;
    }
}
