﻿using System;

namespace LinkLib
{
    public class Node<Type>
    {
        public Node(Type item, Node<Type> next)
        {
            Item = item;
            Next = next;
        }

        public void Append(Type item)
        {
            Node<Type> nextNode = this;
            while(nextNode.Next != null)
            {
                nextNode = nextNode.Next;
            }
            nextNode.Next = new Node<Type>(item, null);
        }

        public Node<Type> Add(Type item)
        {
            Node<Type> newList =
                new Node<Type>(item, this);

            return newList;
        }

        public Type Item { get; private set; }
        public Node<Type> Next { get; set; }
    }
}
