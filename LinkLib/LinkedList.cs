﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace LinkLib
{

    public class SomeClass : IEnumerator
    {
        public object Current => throw new NotImplementedException();

        public bool MoveNext()
        {
            throw new NotImplementedException();
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }
    }

    public class LinkedList<Type> : IEnumerable<Type>
    {
        public LinkedList()
        {
            firstNode = null;
        }

        public virtual void Add(Type newItem)
        {
            Node<Type> newNode =
                new Node<Type>(newItem, firstNode);

            firstNode = newNode;
        }


        // Remove the first node that has the valueToRemove
        // Do nothing if value is not in the list.
        public virtual void Remove(Type valueToRemove)
        {
            // Only remove from a list with elements in it.
            if (firstNode != null)
            {
                if (firstNode.Item.Equals(valueToRemove))
                {
                    firstNode = firstNode.Next;
                }
                else
                {
                    // The first node is not the node to remove.
                    Node<Type> previousCursor = firstNode;
                    Node<Type> cursor = firstNode.Next;

                    bool itemFound = false;
                    while(cursor != null && !itemFound)
                    {
                        if (cursor.Item.Equals(valueToRemove))
                        {
                            itemFound = true;
                            previousCursor.Next =
                                cursor.Next;
                        }
                        else
                        {
                            previousCursor = cursor;
                            cursor = cursor.Next;
                        }
                    }
                }
            }
        }

        public virtual void Append(Type newItem)
        {
            // Create the new node with null next
            Node<Type> newNode =
                new Node<Type>(newItem, null);

            // Check for a null linked-list
            if (firstNode == null)
            {
                firstNode = newNode;
            }
            else
            {
                // Find the last node in the list
                Node<Type> cursor = firstNode;
                while(cursor.Next != null)
                {
                    cursor = cursor.Next;
                }

                // Cursor references the last node in the list.
                cursor.Next = newNode;
            }
        }

        IEnumerator<Type> IEnumerable<Type>.GetEnumerator()
        {
            Node<Type> cursor = firstNode;
            while(cursor != null)
            {
                yield return cursor.Item;
                cursor = cursor.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            Node<Type> cursor = firstNode;
            while(cursor != null)
            {
                yield return cursor.Item;
                cursor = cursor.Next;
            }
        }

        public int Count
        {
            get
            {
                int count = 0;
                Node<Type> currentNode = firstNode;
                while(currentNode != null)
                {
                    ++count;
                    currentNode = currentNode.Next;
                }
                return count;
            }
        }

        protected Node<Type> firstNode;
    }
}
