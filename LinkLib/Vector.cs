﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace LinkLib
{
    public class Vector<Type> : IEquatable<Vector<Type>>,
        IEnumerable<Type>
    {
        public Vector()
        {
            theVector = new List<Type>();
        }

        public Type this[int index]
        {
            get { return theVector[index]; }
            set { theVector[index] = value; }
        }


        public void Add(Type newItem)
        {
            theVector.Add(newItem);
        }

        public int Length()
        {
            return theVector.Count;
        }

        // See if two vectors are equal
        bool IEquatable<Vector<Type>>.Equals(Vector<Type> other)
        {
            bool areEqual = true;
            if (Length() != other.Length()) areEqual = false;


            int index = 0;
            /// While we haven't found the elements to be 
            /// unequal and we haven't reached the end
            /// of the array.
            while (areEqual && index < Length())
            {
                // See if the next elements are the same.
                areEqual = this[index].Equals(other[index]);

                // Move to the next element.
                ++index;
            }

            // return the result
            return areEqual;
        }

        // IEnumerable is an interface that allows
        // a class to be used with foreach

        // To implement IEnumerable we implement
        //  GetEnumerator which returns a class
        //   that implement IEnumertor.


        IEnumerator<Type> IEnumerable<Type>.GetEnumerator()
        {
            // 3rd solution 
            // use a hand-code IEnumerator
            return new ForwardVectorEnumerator<Type>(this);

            // 2nd solution
            //for(int i=Length()-1; i>=0;i--)
            //{
            //    yield return theVector[i];
            //}

            // 1st solution
            // return theVector.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            // 2nd solution
            for (int i = Length() - 1; i >= 0; i--)
            {
                yield return theVector[i];
            }

            // 1st solution
            // return theVector.GetEnumerator();
        }

        List<Type> theVector;
    }
}
