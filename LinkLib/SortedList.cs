﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinkLib
{
    class SortedList<Type> : LinkedList<Type> 
        where Type : IComparable<Type>
    {
        public override void Add(Type newItem)
        {
            Node<Type> newNode = new Node<Type>(newItem, null);

            // If the list is empty
            if (firstNode == null)
            {
                firstNode = newNode;
            }
            else
            {
                // See if the newNode will be the first
                // node in the list.
                if (firstNode.Item.CompareTo(newItem) < 0)
                {
                    newNode.Next = firstNode;
                    firstNode = newNode;
                }
                else
                {
                    // The place for this item is not at
                    // the start of the list.
                    Node<Type> previous = firstNode;
                    Node<Type> current = firstNode.Next;
                    while(current != null && 
                        current.Item.CompareTo(newItem) < 0)
                    {
                        current = current.Next;
                        previous = previous.Next;
                    }

                    // When we get here previous.next should
                    // point to the new item.  And the 
                    // newNode.Next should point to current.
                    previous.Next = newNode;
                    newNode.Next = current;
                }
            }
        }

        public override void Append(Type newItem)
        {
            Add(newItem);
        }


    }
}
