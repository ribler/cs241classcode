﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinkLib
{
    public class Queue<Type>
    {
        // Define the operations that can be performed
        // on a Queue

        // Create a Queue
        public Queue()
        {
            theQueue = null;
            theEnd = null;
            Count = 0;
        }

        public void Enqueue(Type thingToAdd)
        {
            if (theQueue == null)
            {
                theQueue = new Node<Type>(thingToAdd, null);
                theEnd = theQueue;
            }
            else
            {
                //// Find the end of the queue
                //Node<Type> theEnd = theQueue;
                //while(theEnd.Next != null)
                //{
                //    theEnd = theEnd.Next;
                //}
                // theEnd points to the last item in
                // the queue.

                theEnd.Next = 
                    new Node<Type>(thingToAdd, null);

                theEnd = theEnd.Next;
            }
            ++Count;
        }

        public Type Dequeue()
        {
            Type thingToReturn = theQueue.Item;
            theQueue = theQueue.Next;

            // If the queue is null set theEnd
            // to null.
            if (theQueue == null)
            {
                theEnd = null;
            }

            --Count;
            return thingToReturn;
        }

        public int Count
        {
            get { return Count; }

            // myClass.Count = 10;
            set { Count = value; }
        }

        public bool IsEmpty
        {
            get { return Count == 0; }

        }

        // theQueue references the first item in the
        // queue or null for an empty queue.
        private Node<Type> theQueue;

        // theEnd references the last item in the 
        // queue or null for an empty queue.
        private Node<Type> theEnd;
        
    }
}
