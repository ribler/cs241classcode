﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TreeLib;

namespace TreeLibTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void createATree()
        {
            BinaryTree<int, string> myTree =
                new BinaryTree<int, string>();
            Assert.IsTrue(myTree.IsEmpty());
        }

        [TestMethod]
        public void insertIntoEmptyTree()
        {
            BinaryTree<int, string> myTree =
                new BinaryTree<int, string>();
            myTree.Insert(10, "hello");
            Assert.IsFalse(myTree.IsEmpty());
        }

        [TestMethod]
        public void testTraversal()
        {
            BinaryTree<string, int> myTree =
                new BinaryTree<string, int>();
            myTree.Insert("u", 10);
            myTree.Insert("n", 11);
            myTree.Insert("i", 12);
            myTree.Insert("v", 13);
            myTree.Insert("e", 14);
            myTree.Insert("r", 15);
            myTree.Insert("s", 16);
            myTree.Insert("t", 17);
            myTree.Insert("y", 18);
            myTree.Insert("o", 19);
            myTree.Insert("f", 20);
            myTree.Insert("l", 21);
            myTree.Insert("c", 22);
            myTree.Insert("h", 23);
            myTree.Insert("b", 24);
            myTree.Insert("g", 25);
        }
    }
}
