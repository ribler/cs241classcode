﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sorts
{
    class Program
    {
        public static void exchange(int[] array,
            int firstIndex, int secondIndex)
        {
            int temp = array[firstIndex];
            array[firstIndex] = array[secondIndex];
            array[secondIndex] = temp;
        }

        public static void insertionSort(int[] thingToSort)
        {
            int n = thingToSort.Length;

            // Place a sentinel at the first position in 
            // the array (the smallest item in the array.)
            int smallest = thingToSort[0];
            int smallestIndex = 0;
            for(int i=1;i<n;i++)
                if (smallest > thingToSort[i])
                {
                    smallest = thingToSort[i];
                    smallestIndex = i;
                }

            // Place the sentinel as the first item
            // in the list.
            exchange(thingToSort, 0, smallestIndex);

            // For all lengths starting with one
            for(int i=1;i<n;i++)
            {
                // Place the next element in the sorted position
                // for the array of length i+1

                // This is the next value that we are
                // sorting.
                int insertingValue = thingToSort[i];

                int j;
                for(j=i-1; thingToSort[j] > insertingValue;
                    j--)
                {
                    thingToSort[j + 1] = thingToSort[j];
                }
                thingToSort[j + 1] = insertingValue;
            }
        }

        public static void selectionSort(int[] thingToSort)
        {
            int n = thingToSort.Length;

            // For every position in the array starting 
            // with the first location.
            for (int i = 0; i < n; i++)
            {
                // Set the smallest seen so far to min
                int indexOfSmallest = i;
                for (int j = i + 1; j < n; j++)
                {
                    if (thingToSort[j] <
                        thingToSort[indexOfSmallest]) indexOfSmallest = j;
                }
                exchange(thingToSort, i, indexOfSmallest);
            }
        }
        static void Main(string[] args)
        {
            int[] thingToSort = new int[] { 3, 5, 1, 6, 2, 8, 10 };
            selectionSort(thingToSort);
            if(isSorted(thingToSort))
            {
                Console.WriteLine("It is sorted!");
            }


            int[] anotherThing = new int[] { 3, 5, 1, 6, 2, 8, 10 };
            insertionSort(anotherThing);
            if(isSorted(anotherThing))
            {
                Console.WriteLine("It is sorted!");
            }
            Console.ReadLine();

        }

        private static bool isSorted(int[] thingToSort)
        {
            bool isSorted = true;
            for(int i=1;i<thingToSort.Length;i++)
            {
                if (thingToSort[i] < thingToSort[i - 1]) isSorted = false;
            }
            return isSorted;
        }
    }
}
