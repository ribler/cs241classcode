﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sorts
{
    public class Heap<Type> where Type : IComparable
    {
        public Heap(Type[] someArray)
        {
            theArray = someArray;

            // To reference the first item
            // this[1] = this[2];
        }

        // We want to use indexes that start at 1
        // rather than 0.
        public ref Type this[int index]
        {
            get
            {
                return ref theArray[index - 1];
            }
        }

        Type[] theArray;
    }
}
