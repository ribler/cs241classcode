﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardLib
{
    public class Card : IComparable<Card>, IEquatable<Card>
    {
        // What operations can we perform on Card?

        public enum SuitType { Clubs, Diamonds, Hearts, Spades, Joker};
        public const int JOKER_RANK = 50;
        
        // Create a new Card
        public Card(int rank, SuitType suit)
        {
            Rank = rank;
            Suit = suit;
            if (Suit == SuitType.Joker)
            {
                Rank = JOKER_RANK;
            }
        }

        // Create a copy of a card
        public Card(Card original)
        {
            Rank = original.Rank;
            Suit = original.Suit;
        }

        // Determine if a card is a joker.
        public bool IsJoker { get; private set; }

        // Determine the rank
        // Yields an int [2, 13]   // joker is rank 0
        public int Rank { get; private set; }

        // Determine the suit
        // Yields an enumeration type in the range {Clubs, Diamonds, Hearts, Spades, Joker}
        public SuitType Suit { get; private set; }

        // Provides a print name for the card.
        public override string ToString()
        {
            string rankName = RankToString();
            if (Rank != JOKER_RANK)
            {
                rankName = rankName + " of " + Suit.ToString();
            }

            return rankName;
        }

        private string RankToString()
        {
            string rankName;
            switch(Rank)
            {
                case 11:
                    rankName = "Jack";
                    break;

                case 12:
                    rankName = "Queen";
                    break;

                case 13:
                    rankName = "King";
                    break;

                case JOKER_RANK:
                    rankName = "Joker";
                    break;

                case 1:
                    rankName = "Ace";
                    break;

                default:
                    rankName = Rank.ToString();
                    break;
            }

            return rankName;
        }

        // Compare cards for equality 
        public virtual bool Equals(Card rhs)
        {
            return Rank == rhs.Rank && Suit == rhs.Suit;
        }

        // Determine the ordinal relationship between two cards
        public virtual int CompareTo(Card rhs)
        {
            int returnValue;
            if (Rank != rhs.Rank)
            {
                returnValue = Rank.CompareTo(rhs.Rank);
            }
            else
            {
                returnValue = Suit.CompareTo(rhs.Suit);
            }
            return returnValue;
        }

    }
}
