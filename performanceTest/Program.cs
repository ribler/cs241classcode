﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace perform
{
    // Consider a program with an inner loop
    // that executes n^2

    // Say n = 1000
    //   n^2 = 1,000,000 = 10^6 * 10^-8 = 10^-2 (1/100 sec)

    // Say n = 1,000,000
    //  n^2 = 10^6 * 10^6 = 10^12 * 10^-8 = 10^4  (2-3 hours)

    // Say n = 1,000,000,000 
    //  n^2 = 10^9*10^9 = 10^18 * 10^-1 = 10^10 (10 years)

    // seconds/hour  = 3600
    // seconds/day = 86,400
    // seconds/week 604,800
    // seconds/year = approx 31,557,600 (between 10^8 and 10^9)
    
    // Consider a program that has a loop in which instructions
    // are executed n^3 times.

    // Say n = 1000 
    // n^3 = 10^3*10^3*10^3 = 10^9; 10^9 * 10^-8 = 10

    // Say n = 10000
    // n^3 = 10^4*10^4*10^4 = 10^12 ;; 10^12 * 10^-8 = 10^4 

    // Say n = 1000000
    // n^3 = 10^6*10^6*10^6 = 10^18; 10^18 * 10^-8 = 10^10;

    // Tilde notation
    // Disregard terms that are not very significant
    // g(n) = n^3/6 + n^2/2 + n/3
    // ~f(n) = n^3
    // g(n)/~f(n) approaches 1 as n gets large.

    class Program
    {
        static void Main(string[] args)
        {

            const int N = 100000;

            int[] a = new int[N];
            int[] b = new int[N];
            int[] c = new int[N];


            int seed = 23452111;
            Random rand = new Random(seed);

            for (int i = 0; i < N; i++)
            {
                a[i] = rand.Next();
                b[i] = rand.Next();
                c[i] = rand.Next();
            }

            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    c[i] = a[j] - b[i];
                }
            }
            Console.WriteLine("Done with n squared.");


            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    for (int k = 0; k < N; k++)
                    {
                        c[i] += a[j] - b[k];
                    }
                }
            }
            Console.WriteLine("Done with n cubed.");
            Console.ReadLine();
        }
    }
}