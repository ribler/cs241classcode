﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace funWithCSharp
{
    public class Vector<Type>
    {
       public Vector(int nItems)
        {
            theArray = new Type[nItems];
        }

        // Implementing the bracket operator
        // We use a C# Property for [] operator
        public Type this[int index]
        {
            get { return theArray[index]; }
            set { theArray[index] = value; }
        }

        private Type[] theArray = null;
    }
}
