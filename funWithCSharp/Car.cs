﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//interface ISomeClass
//{
//    int someFunc();
//    string someOther(int x, double y);
//};

namespace funWithCSharp
{
    public class Car : IComparable, IEquatable<Car>
    {
        public Car(int nDoors)
        {
            NDoors = nDoors;
        }

        public bool Equals(Car rhsCar)
        {
            return NDoors == rhsCar.NDoors;
        }

        public static bool operator==(Car lhs, Car rhs)
        {
            return lhs.Equals(rhs);
        }

        public static bool operator<(Car lhs, Car rhs)
        {
            return lhs.CompareTo(rhs) < 0;
        }

        public static bool operator>(Car lhs, Car rhs)
        {
            return lhs.CompareTo(rhs) > 0;
        }
        

        public static bool operator!=(Car lhs, Car rhs)
        {
            return !lhs.Equals(rhs);
        }



        public int CompareTo(object obj)
        {
            // Compare Cars only to other Cars.
            Car rhsCar = (Car) obj;

            int returnValue = 0;
            if (NDoors < rhsCar.NDoors) returnValue = -1;
            if (NDoors > rhsCar.NDoors) returnValue = 1;

            return returnValue;
        }

        // Compute the time it takes to wash
        public virtual float MinutesToWash()
        {
            const float DOOR_WASH_CONSTANT = 4.0f;
            const float BODY_WASH_CONSTANT = 10.0f;
            return
            NDoors * DOOR_WASH_CONSTANT +
            BODY_WASH_CONSTANT;
        }

        public static float mpg(float miles, float gallons)
        {
            return miles / gallons;
        }

        public Car(Car orig)
        {
            NDoors = orig.NDoors;
        }

        //public int getNDoors()
        //{
        //    return NDoors;
        //}

        public int NDoors { get; set; }
    }
}
