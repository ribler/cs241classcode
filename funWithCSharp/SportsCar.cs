﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace funWithCSharp
{
    class SportsCar : Car
    {
        SportsCar() : base(2)
        {
        }

        public override float MinutesToWash()
        {
            const float SPORTS_CAR_WASH_FACTOR = 5.5F;
            return base.MinutesToWash() *
                SPORTS_CAR_WASH_FACTOR;
        }

        // Sports cars maintain a list of awards
        public List<string> AwardList;
    }
}
