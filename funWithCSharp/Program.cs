﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows;

// Leap year rule
//  A year is a leap year if 
//  divisible by 4 and not divisible by 100 
//  or if it is divisible by 400

// Day of the week 
// (Number of years since 1600 +
// Number of leap years since 1600 +
// day of the month +
// month offset for month
// + 6 (Jan 1, 1600 was a Saturday)) % 7
// if the current year is a leap year and it is March 1 or later
// add 1 to the sum and then % 7
// 0 = Sunday
// 6 = Saturday

namespace funWithCSharp
{
    class Program
    {
        enum CarTypes { Ford = 10, Toyota = 20, Nissan = 25};

        // Pass-by-value example
        //   Allocate memory to make copy
        //   Copy the value passed-in
        // static int Add(int& firstOp, int& secondOp);

        static int Add(int firstOp, int secondOp)
        {
            return firstOp += secondOp;
        }

        static int IntDivide(int dividend,
            int divisor, out int remainder)
        {
            remainder = dividend % divisor;
            return dividend / divisor;
        }



        static void Main(string[] args)
        {
            CarTypes myCarType = CarTypes.Nissan;
            Console.WriteLine("My car type is {0}", myCarType.ToString());
            int myCarYear = 2001;
            string yearMyCarWasBuilt = myCarYear.ToString();
            int theIntAgain = Int32.Parse(yearMyCarWasBuilt);

            StreamReader reader = new StreamReader("sampleTest.txt");

            while (reader.Peek() > 0)
            {
                string line = reader.ReadLine();
                // sp, comma, semi, ., \t
                const int N_SEPARATORS = 5;
                char[] wordSeparators = new char[N_SEPARATORS];
                wordSeparators[0] = ' ';
                wordSeparators[1] = ',';
                wordSeparators[2] = ';';
                wordSeparators[3] = '.';
                wordSeparators[4] = '\t';

                string[] words = 
                    line.Split(wordSeparators, 
                    StringSplitOptions.RemoveEmptyEntries);

                for(int i=0;i<words.Length;i++)
                {
                    Console.WriteLine(words[i]);
                }

                // Console.WriteLine(line);
            }
            reader.Close();

            // Open a file with int data
            StreamReader intReader = new StreamReader("sampleInt.txt");


            // Create a list of integers
            List<int> intList = new List<int>();

            while (intReader.Peek() > 0)
            {
                string line = intReader.ReadLine();
                // sp, comma, semi, ., \t
                const int N_SEPARATORS = 5;
                char[] intSeparators =
                    new char[N_SEPARATORS] { ' ', ',', ';', '.', '\t' };
                //char[] wordSeparators = new char[N_SEPARATORS];
                //wordSeparators[0] = ' ';
                //wordSeparators[1] = ',';
                //wordSeparators[2] = ';';
                //wordSeparators[3] = '.';
                //wordSeparators[4] = '\t';

                string[] intsRead = 
                    line.Split(intSeparators, 
                    StringSplitOptions.RemoveEmptyEntries);

                // Add the int list of ints
                for(int i=0;i<intsRead.Length;i++)
                {
                    intList.Add(Int32.Parse(intsRead[i]));
                }
            }
            Console.WriteLine("Integers in the list are:");
            for(int i=0;i<intList.Count;i++)
            {
                Console.WriteLine("{0}", intList[i]);
            }

            Console.WriteLine("Write the ints using foreach");
            foreach(int listItem in intList)
            {
                Console.WriteLine("{0}", listItem);
            }

            intReader.Close();

            Vector<int> myVector = new Vector<int>(10);
            myVector[7] = 20;
            Console.WriteLine("Vector element is: {0}", myVector[7]);

            // string args[SIZE];
            int nParams = 2;
            Console.WriteLine(
                "There are {0} parameters, we expected {1}",
                args.Length, nParams);
            Console.WriteLine("Hello C#!!!");

            // Declaring a reference to a Car
            // But I have 0 car objects.
            Car myCar;
            
            // The reference is referencing the new Car
            myCar = new Car(4);
            Car yourCar = new Car(myCar);

            Console.WriteLine("myCar has {0} doors.",
                myCar.NDoors);

            myCar.NDoors = 2;

            // Call a static function to determine mpg
            double miles = 100.0;
            Console.WriteLine("mpg is {0}",
            Car.mpg((float) miles, 3.0f));

            // Create an array of integers of size n
            int n = 100;
            int[] myArray = new int[n];

            // Fill the array with values 0 to 99
            for (int i = 0; i < myArray.Length; i++)
            {
                myArray[i] = i;
            }

            int x = 10;
            int y = 21;
            int z = Add(x, y);

            if (x == z)
            {
                Console.WriteLine("x and z are the same.");
            }

            int remainder;
            int quotient = IntDivide(y, x, out remainder);
            Console.WriteLine("Quotient is {1} Remainder is {0}",
                remainder, quotient);

            Car someCar = new Car(4);
            Car someOtherCar = new Car(4);
            myCar = someCar;

            // 
            if (someCar.Equals(someOtherCar))
            {
                Console.WriteLine("Cars are the same");
            }
            else
            {
                Console.WriteLine("Cars are different");
            }

            if (someCar == someOtherCar)
            {
                Console.Write("someCar and someOtherCar have the same number of doors!");
            }

            Console.ReadLine();
        }
    }
}
