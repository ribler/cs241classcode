﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinkLib;
using System.IO;

namespace breadthFirstDir
{
    class Program
    {

        // 
        //void someFunction()
        //{
        //    // 100, 000 lines of code above here.
        //    x = x / 2;
        //    x = x >> 1;
        //    for(int i=0;i<n;i++)
        //    {
        //        // <line 5>
        //        // <line 6>
        //        // <line 7>
        //        for (int j = 0; j < n; j++)
        //        {
        //            // <line 1>
        //            // <line 2>
        //            // <line 3>
        //            // <line 4>
        //        }
        //    }
        //}

        static void Main(string[] args)
        {
            Tuple<int, DirectoryInfo> myTuple =
                new Tuple<int, DirectoryInfo>(1,
                new DirectoryInfo("C:\\altera"));

            int theInt = myTuple.Item1;
            DirectoryInfo theDir = myTuple.Item2;

            Queue<Tuple<int, DirectoryInfo>>
                dirQueue = new
                Queue<Tuple<int, DirectoryInfo>>();

            dirQueue.Enqueue(
                new
                Tuple<int, DirectoryInfo>(1, new DirectoryInfo("C:\\altera")));

            int maxLevel = 3;
            int currentLevel = 1;

            // Algorithm #1
            // For this algorithm for Enqueue
            //  Start with the first element and find the end
            //    Check to see if the end has been reached
            //    if not move to the next link

            // Call the time it takes to check and move to the
            // next element "c"
            
            // How long will it take to insert the item?
            //    Let's say there are n items in the queue.
            //  c*N

            // How does the time change as a function of n?
            //   f(n) = c*n



            //  Add the element to the end
            // 

            // Algorithm #2
            //  Add the element to where endQueue points
            //  update endQueue
            //  f(n) = c




            Tuple<int, DirectoryInfo> currentTuple;
            do
            {
                // Pull the next item out of the queue.
                currentTuple = dirQueue.Dequeue();

                if (currentTuple.Item1 <= maxLevel)
                {
                    if (currentTuple.Item1 > currentLevel)
                    {
                        Console.WriteLine();
                        currentLevel = currentTuple.Item1;
                        Console.Write("{0}. ", currentLevel);
                    }
                    Console.Write("{0}-", currentTuple.Item2.Name);
                }

                // Add the subdirectories to the queue.
                foreach (DirectoryInfo dirInfo in currentTuple.Item2.GetDirectories())
                {
                    // Add subdir to queue
                    dirQueue.Enqueue(
                        new Tuple<int, DirectoryInfo>(currentTuple.Item1 + 1,
                        dirInfo));
                }

            } while (currentTuple.Item1 <= maxLevel && 
                   !dirQueue.IsEmpty);
            Console.ReadLine();
        }

    }
}
