﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test1SampleQuestions
{
    // C# vs C++
    //  Car myCar;  // How is this statement different in C# vs C++
    //  Car myCar = new Car();

    // Assume myCar specifies an existing object.
    // Car yourCar = myCar;

    // What is an interface?  What is the closest 
    // entity in C++ that can be used as an interface?

        // In C#
    //interface IMyInterface
    //{
    //    void someFunction(int x);
    //}

    ////  In C++
    //class IMyInterface
    //{
    //    void someFunction(int x) = 0;
    //}

    // What is an  Abstract Data Type?
    // An Abstract Data Type is a data type
    // that is defined strictly in terms
    // of the operations that it provides.

    // Know about some specific interfaces
    //  IComparable
    //  IEnumerable
    //  IEnumerator

    // Know about generic classes
    //  Write a generic class that implements a stack.

    // Implement something with a linked-list
    //   Why use a linked-list rather than an array?
    //   When is one better than the other. (Advantages/Disadvantages)

    // Recursive Functions
    //
    //  1) We can reduce any non-base case 
    //     problem with an operation and a
    //     recursive call on a smaller problem.
    //  2) We can identify a termination condition
    //     where the solution is trivial or 
    //     unnecessary.

    //  Write a recursive function that prints a list in 
    //  reverse order. Do not copy or change the
    //  order of the list itself.
    //  void printInReverse<List<Type> thingToPrint)
    //
    //  Write a recursive function that counts the
    //  number of squares in a maze.

    //  Time complexity of algorithms
    //     Given an example algorithm, describe how
    //  the time required might increase as N increases.
    //
    //  Often the level of nesting is a good clue
    //  for(int i=0;i<n;i++)
    //  {
    //      for(int j=0;j<n/2;j++)
    //      {
    //          for(int k=0;k<n;k++)
    //          {
    //          }
    //      }
    //  }
    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
