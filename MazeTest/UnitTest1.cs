﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MazeLib;

namespace MazeTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void createBox()
        {
            // Create a Box
            Box myBox = new Box(0,0);
            Assert.IsTrue(myBox.hasEastWall);
            Assert.IsTrue(myBox.hasWestWall);
            Assert.IsTrue(myBox.hasSouthWall);
            Assert.IsTrue(myBox.hasNorthWall);
        }

        [TestMethod]
        public void createMaze()
        {
            int nRows = 3;
            int nColumns = 2;
            Maze myMaze = new Maze(nRows, nColumns);
            Assert.AreEqual(nRows, myMaze.NRows);
            Assert.AreEqual(nColumns, myMaze.NColumns);
        }

        [TestMethod]
        public void getBoxFromMaze()
        {
            int nRows = 3;
            int nColumns = 2;
            Maze myMaze = new Maze(nRows, nColumns);
            Box lastBox = myMaze[nRows - 1, 
                nColumns - 1];
            Assert.IsFalse(lastBox.hasBeenVisited);
        }

        public void makeMaze()
        {
            int nRows = 3;
            int nColumns = 2;
            Maze myMaze = new Maze(nRows, nColumns);
            myMaze.makeMaze();
            for(int row = 0; row<nRows; ++row)
            {
                for (int column = 0; column <nColumns; ++column)
                {
                    Assert.IsTrue(myMaze[row, column].hasBeenVisited);
                }
            }
        }

    }
}
