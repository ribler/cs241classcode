﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeLib
{
    public class Box
    {
        public Box(int row, int column)
        {
            Row = row;
            Column = column;
        }

        public bool hasBeenVisited { get; set; } = false;
        public bool hasNorthWall { get; set; } = true;
        public bool hasSouthWall { get; set; } = true;
        public bool hasEastWall { get; set; } = true;
        public bool hasWestWall { get; set; } = true;

        public int Row { get; set; }
        public int Column { get; set; }
    }
}
