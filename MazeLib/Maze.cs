﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeLib
{
    // Algorithm to build a rectangular maze
    //   Make a grid of nRowsxnColumns Boxes
    //      each box has 4 walls (North, South, East, West)
    //      each box can be marked "visited" (boolean)
    //  Make the top-left box the initial box
    //  Mark the box as visited.  Make this box the current box
    //  While there are unvisited boxes
    //     if the current box has unvisited neighbors
    //         Choose a randomly select unvisited neighbor
    //         Push the current box on a stack
    //         Remove the walls between the current box
    //         and the chosen box.
    //         Make the chosen box the current box and
    //         mark it visited.
    //     else
    //          Pop a box from the stack
    //          Make the popped box the current cell.

    public class Maze
    {
        public Maze(int nRows, int nColumns)
        {
            NRows = nRows;
            NColumns = nColumns;
            maze = new Box[nRows, nColumns];
            for (int row = 0; row < nRows; ++row)
            {
                for (int column = 0; column < nColumns; ++column)
                {
                    maze[row, column] = new Box(row, column);
                }
            }
        }

        private void RemoveWallsBetween(Box firstBox,
            Box secondBox)
        {
            // If they are in the same column
            // Break a horizontal wall
            if (firstBox.Column == secondBox.Column)
            {
                // If first box is above the second
                if (firstBox.Row < secondBox.Row)
                {
                    firstBox.hasSouthWall = false;
                    secondBox.hasNorthWall = false;
                }
                else
                {
                    secondBox.hasSouthWall = false;
                    firstBox.hasNorthWall = false;
                }
            }
            else
            {
                if (firstBox.Row == secondBox.Row)
                {
                    if (firstBox.Column < secondBox.Column)
                    {
                        firstBox.hasEastWall = false;
                        secondBox.hasWestWall = false;
                    }
                    else
                    {
                        secondBox.hasEastWall = false;
                        firstBox.hasWestWall = false;
                    }
                }
            }
        }

        public void makeMaze()
        {
            // Algorithm to build a rectangular maze
            //   Make a grid of nRowsxnColumns Boxes
            //      each box has 4 walls (North, South, East, West)
            //      each box can be marked "visited" (boolean)
            //  Make the top-left box the initial box
            //  Mark the box as visited.  Make this box the current box
            //  While there are unvisited boxes
            //     if the current box has unvisited neighbors
            //         Choose a randomly select unvisited neighbor
            //         Push the current box on a stack
            //         Remove the walls between the current box
            //         and the chosen box.
            //         Make the chosen box the current box and
            //         mark it visited.
            //     else
            //          Pop a box from the stack
            //          Make the popped box the current cell.

            /// Set topleft box to visited
            int currentRow = 0;
            int currentColumn = 0;
            maze[currentRow, currentColumn].hasBeenVisited = true;
            int nBoxesVisited = 1;
            int nBoxesInMaze = NRows * NColumns;
            pathFromTop.Push(maze[currentRow, currentColumn]);
            Random randomGen = new Random(234251);

            while (nBoxesVisited < nBoxesInMaze)
            {
                Box nextBox;
                Box poppedBox = null;

                List<Box> unvisitedNeighbors;
                // find a random unvisited neighbor to current.
                do
                {
                    unvisitedNeighbors =
                        GetUnvisitedNeighbors(currentRow,
                        currentColumn);

                    if (unvisitedNeighbors.Count == 0)
                    {
                        poppedBox = pathFromTop.Pop();
                        currentRow = poppedBox.Row;
                        currentColumn = poppedBox.Column;
                    }
                } while (unvisitedNeighbors.Count == 0);

                if (poppedBox != null)
                {
                    pathFromTop.Push(poppedBox);
                }

                // We have unvisited neighbors
                // Pick a random unvisited neighbor
                nextBox =
                    unvisitedNeighbors[randomGen.Next()
                    % unvisitedNeighbors.Count];

                ++nBoxesVisited;

                RemoveWallsBetween(maze[currentRow, currentColumn],
                    nextBox);

                nextBox.hasBeenVisited = true;

                // Push the box on the stack
                pathFromTop.Push(nextBox);

                // Check to see if we are at the
                // destination.
                if (nextBox.Row == NRows - 1 &&
                    nextBox.Column == NColumns - 1)
                {
                    // This constructor will use
                    // Stack<Type>(IEnumerable origValue)
                    result = new Stack<Box>(pathFromTop);
                }
                currentRow = nextBox.Row;
                currentColumn = nextBox.Column;
            }

        }

        public void PrintSolution()
        {
            // Save the stack
            Stack<Box> savedStack = new Stack<Box>(result);

            // Print all the value in the result stack
            Console.WriteLine("Solution: ");
            while (result.Count > 0)
            {
                Box nextBox = result.Pop();
                Console.WriteLine("({0}, {1})",
                    nextBox.Row, nextBox.Column);
            }

            // Restore the result stack
            result = new Stack<Box>(savedStack);
        }

        public void PrintDirections()
        {
            // Save the stack
            Stack<Box> savedStack = new Stack<Box>(result);

            // Print all the value in the result stack
            Console.WriteLine("Solution: ");

            Box currentBox = result.Pop();
            while (result.Count > 0)
            {
                Box nextBox = result.Pop();
                if (nextBox.Row == currentBox.Row)
                {
                    if (currentBox.Column < nextBox.Column)
                    {
                        Console.WriteLine("Right");
                    }
                    else
                    {
                        Console.WriteLine("Left");
                    }
                }
                else
                {
                    if (currentBox.Row < nextBox.Row)
                    {
                        Console.WriteLine("Down");
                    }
                    else
                    {
                        Console.WriteLine("Up");
                    }
                }
                currentBox = nextBox;
            }
            // Restore the result stack
            result = new Stack<Box>(savedStack);
        }

        public void Print()
        {
            // For every row of boxes
            for (int row = 0; row < NRows; ++row)
            {
                Console.Write("+");
                for (int column = 0; column < NColumns; ++column)
                {
                    if (maze[row, column].hasNorthWall)
                    {
                        Console.Write("-+");
                    }
                    else
                    {
                        Console.Write(" +");
                    }
                }
                Console.Write("\n");

                // Print box sides

                // Print the left wall
                Console.Write("|");
                for (int column = 0; column < NColumns; ++column)
                {
                    if (maze[row, column].hasEastWall)
                    {
                        Console.Write(" |");
                    }
                    else
                    {
                        Console.Write("  ");
                    }
                }

                Console.Write("\n");
            }

            for (int column = 0; column < NColumns; ++column)
            {
                if (maze[NRows - 1, column].hasSouthWall)
                {
                    Console.Write("-+");
                }
            }
            Console.WriteLine();
        }

        private List<Box> GetUnvisitedNeighbors(int row,
            int column)
        {
            // Create a new list 
            List<Box> unvistedList = new List<Box>();

            if ((row - 1) >= 0 &&
                !maze[row - 1, column].hasBeenVisited)
                unvistedList.Add(maze[row - 1, column]);

            if ((column - 1) >= 0 &&
                !maze[row, column - 1].hasBeenVisited)
                unvistedList.Add(maze[row, column - 1]);

            if ((row + 1) < NRows &&
                !maze[row + 1, column].hasBeenVisited)
                unvistedList.Add(maze[row + 1, column]);

            if ((column + 1) < NColumns &&
                !maze[row, column + 1].hasBeenVisited)
                unvistedList.Add(maze[row, column + 1]);

            return unvistedList;
        }

        public Box this[int row, int column]
        {
            get
            {
                return maze[row, column];
            }
        }

        Stack<Box> pathFromTop = new Stack<Box>();

        private Box[,] maze;

        private Stack<Box> result;
        public int NRows { get; private set; }
        public int NColumns { get; private set; }
    }
}
