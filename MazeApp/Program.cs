﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MazeLib;

namespace MazeApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Maze myMaze = new Maze(20, 40);
            myMaze.makeMaze();
            myMaze.Print();
            myMaze.PrintSolution();
            myMaze.PrintDirections();
            Console.ReadLine();
        }
    }
}
