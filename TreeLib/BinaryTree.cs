﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TreeLib
{
    // Binary Tree 
    // - all keys that are less than
    //   the parent node will be stored in the
    //   left subtree
    // - all keys that are greater than the
    //   the parent node will be stored in the
    //   right subtree.
    public class BinaryTree<KeyType, ValueType>
        where KeyType : IComparable<KeyType>
    {
        public BinaryTree()
        {
        }

        // Preorder traversal of binary tree
        public IEnumerable<Tuple<KeyType, ValueType>>
            PreorderTraversal()
        {
            List<Tuple<KeyType, ValueType>> preOrderList =
                new List<Tuple<KeyType, ValueType>>();

            if (Root != null)
            {
                Node.PreorderTraversal(Root, preOrderList);
            }

            return preOrderList;
        }

        public class Node
        {
            private Node[] child = new Node[2] { null, null };
            enum Child { Left = 0, Right = 1 };

            public Node(KeyType key,
                ValueType nodeValue)
            {
                Key = key;
                NodeValue = nodeValue;
            }

            public static void
                PreorderTraversal(Node root,
                List<Tuple<KeyType, ValueType>> preOrderList)
            {
                if (root != null)
                {

                    //yield return new 
                    //Tuple<KeyType, ValueType>(root.Key, root.NodeValue));

                    //foreach(Tuple<KeyType, ValueType> node in preOrderList(LeftChild))
                    //{
                    //    yield return node;
                    //}

                    // Visit the root 
                    preOrderList.Add(new 
                    Tuple<KeyType, ValueType>(root.Key, root.NodeValue));

                    // Visit the left subtree
                    PreorderTraversal(root.LeftChild, preOrderList);

                    // Visit the right subtree
                    PreorderTraversal(root.RightChild, preOrderList);
                }
            }


            public static void Insert(Node node, KeyType key,
                ValueType value)
            {
                // if the inserted key is less than the
                // nodes key.
                if (key.CompareTo(node.Key) < 0)
                {
                    if (node.LeftChild == null)
                    {
                        node.LeftChild = new Node(key, value);
                    }
                    else
                    {
                        // insert the new node into the left subtree.
                        Insert(node.LeftChild, key, value);
                    }
                }
                else
                {
                    if (key.CompareTo(node.Key) > 0)
                    {
                        if (node.RightChild == null)
                        {
                            node.RightChild = new Node(key, value);
                        }
                        else
                        {
                            // insert the new node into the left subtree.
                            Insert(node.RightChild, key, value);
                        }
                    }
                    else
                    {
                        // The keys are equal - replace the old value
                        node.NodeValue = value;
                    }
                }
            }

            private KeyType Key { get; set; }
            private ValueType NodeValue { get; set; }

            public Node LeftChild
            {
                get
                {
                    return child[(int)Child.Left];
                }
                set
                {
                    child[(int)Child.Left] = value;
                }
            }

            public Node this[int childNumber]
            {
                get
                {
                    return child[childNumber];
                }
                set
                {
                    child[childNumber] = value;
                }
            }
            public Node RightChild
            {
                get
                {
                    return child[(int)Child.Right];
                }
                set
                {
                    child[(int)Child.Right] = value;
                }
            }
        }

        // BT Insert function.
        public void Insert(KeyType key,
            ValueType value)
        {
            // Check to see if the root is null
            if (Root == null)
            {
                Root = new Node(key, value);
            }
            else
            {
                // Call the node insert function.
                Node.Insert(Root, key, value);
            }
        }

        public bool IsEmpty()
        {
            return Root == null;
        }

        Node Root = null;
    }
}
