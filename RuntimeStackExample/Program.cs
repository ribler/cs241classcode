﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//public class ChoicePoint
//{
//    public ChoicePoint(Maze theMaze,
//        Box theBox)
//    {
//        // Create the list of 
//        // untriedNeighbors
//    }

//    Box theBox;
//    Stack<Box> untriedNeighbors;
//}

namespace RuntimeStackExample
{
    class Program
    {

        static int smallest(IEnumerator<int> enumerator)
        {
            int returnValue = int.MaxValue;
            if (enumerator.MoveNext())
            {
                int current = enumerator.Current;
                int smallestInRest = smallest(enumerator);
                if (current <  smallestInRest)
                {
                    returnValue = current;
                }
                else
                {
                    returnValue = smallestInRest;
                }
            }
            return returnValue;
        }

        static int smallest(List<int> theList)
        {
            IEnumerator<int> enumerator =
                theList.GetEnumerator();

            return smallest(enumerator);
        }

        // Fact(n) n*n-1*n-2...1
        // Fact(0) == 1
        static long Fact(int n)
        {
            long returnResult = 1;
            if (n != 0)
            {
                returnResult =
                    n * Fact(n - 1);
            }
            return returnResult;
        }

        static float myOtherFunc()
        {
            int j = 10;
            return j * 32.0f;
        }
        static int myFunc(int w)
        {
            float a = 1.0f;
            float b = 2.0f;
            float c = myOtherFunc();
            float d = myOtherFunc();
            return 10;
        }

        static void Main(string[] args)
        {
            int x = 4;
            long result = Fact(x);
            List<int> list = new List<int>() { 3, 5, 2, 1, 7 };
            int small = smallest(list);
        }
    }
}
