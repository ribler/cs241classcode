﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MergeSort
{
    public class Merge<Type> where Type : IComparable<Type>
    {
        /// <summary>
        ///  Merge a[lo..mid] // one sorted array with
        ///  a[mid+1..hi] // the sorted array
        /// </summary>
        /// <param name="thingToSort"></param>
        /// <param name=""></param>
        static void merge(Type[] thingToSort, 
            int lo, int mid, int hi)
        {
            // First copy the array

            // Allocate the memory
            //IComparable<Type>[] aux =
             //   new IComparable<Type>[thingToSort.Length];

            for(int k=lo;k<=hi;k++)
            {
                aux[k] = thingToSort[k];
            }

            // index of first array is i
            int i = lo;

            // index of second array is j
            int j = mid + 1;

            for(int k=lo; k <= hi; k++)
            {
                // If we have run out of the first array,
                // just copy from the second array.
                if (i > mid) thingToSort[k] = aux[j++];

                // if we have run out the second array,
                // just copy from the first array
                else if (j > hi) thingToSort[k] = aux[i++];
                else if (aux[j].CompareTo(aux[i]) < 0) thingToSort[k] = aux[j++];
                else thingToSort[k] = aux[i++];
            }
        }

        static Type[] aux;

        static void mergeSort(Type[] thingToSort, int lo, int hi)
        {
            // hi <= lo is the base case and we can just return
            if (hi > lo)
            {
                // lo is the base of the array
                // hi - lo is the length of the array
                // (hi - lo) / 2 is the middle subscript
                int mid = lo + (hi - lo) / 2;
                mergeSort(thingToSort, lo, mid);
                mergeSort(thingToSort, mid + 1, hi);
                merge(thingToSort, lo, mid, hi);
            }

        }

        public static void mergeSort(Type[] thingToSort)
        {
            // Allocate the memory for all merges.
            aux = new Type[thingToSort.Length];
            mergeSort(thingToSort, 0, thingToSort.Length - 1);
        }
    }



    class Program
    {

        // This function works for n if n is a power of  2
        static int nComparesForMergeSortPower2(int n)
        {
            int returnValue = 0;
            if (n > 1)
            {
                returnValue =
                    2 * nComparesForMergeSortPower2(n / 2)
                    + n;
            }
            return returnValue;
        }
        static void Main(string[] args)
        {

            for(int i=2;i<32*1024;i=i*2)
            {
                Console.WriteLine("N = 2: {0} {1}",
                    nComparesForMergeSortPower2(i),
                    i*Math.Log(i, 2));
            }

            Console.ReadLine();

            int[] intsToSort = 
                new int[] { 23, 45, 123, 3, 34, 5, 345, 0 };

            string[] thingToSort = 
                new string[] { "23", "45", "123", "3", "34","5", "345", "0" };

            //  Mergesort algorithm
            //  Base case is one item ( just return)
            //  if n > 1
            //  mergeSort the first half
            //  mergeSort the second half
            //  merge the two halves
            Merge<int>.mergeSort(intsToSort);
            Merge<string>.mergeSort(thingToSort);

            Console.WriteLine("Sorted Ints: ");
            for(int i=0;i<thingToSort.Length;i++)
            {
                Console.WriteLine(intsToSort[i]);
            }

            Console.WriteLine("");
            Console.WriteLine("Sorted Strings: ");
            for(int i=0;i<thingToSort.Length;i++)
            {
                Console.WriteLine(thingToSort[i]);
            }

            Console.ReadLine();
        }
    }
}
